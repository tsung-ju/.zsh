SRCS:=$(wildcard\
	zsh-autosuggestions/zsh-autosuggestions.zsh\
	zsh-autosuggestions/src/*.zsh\
	zsh-autosuggestions/src/strategies/*.zsh\
	zsh-syntax-highlighting/zsh-syntax-highlighting.zsh\
	zsh-syntax-highlighting/highlighters/*/*.zsh\
	zsh-history-substring-search/zsh-history-substring-search.zsh\
	powerlevel10k/*.zsh-theme\
	powerlevel10k/internal/*.zsh)

ZWCS:=$(addsuffix .zwc,$(SRCS))

.PHONY: init upgrade compile clean

init:
	git submodule update --init
	$(MAKE) compile

upgrade:
	git submodule update --remote
	$(MAKE) compile

compile: $(ZWCS)

clean:
	rm -f $(ZWCS)

%.zwc: %
	zsh -c 'zcompile -R -- $$0 $$1' $@ $<
