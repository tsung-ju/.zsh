## Prerequisites
- Make
- Git
- Zsh

## Install
```sh
git clone https://github.com/tsung-ju/.zsh ~/.zsh
make -C ~/.zsh init
echo 'export ZDOTDIR=~/.zsh' >> ~/.zshenv
```

## Update
```sh
make -C ~/.zsh upgrade
```
