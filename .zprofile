export EDITOR=vim

SHELL_SESSIONS_DISABLE=1
export CLICOLOR=1

# Homebrew
export HOMEBREW_PREFIX="/opt/homebrew";
export HOMEBREW_CELLAR="/opt/homebrew/Cellar";
export HOMEBREW_REPOSITORY="/opt/homebrew";
export PATH="${HOMEBREW_PREFIX}/bin:${HOMEBREW_PREFIX}/sbin${PATH+:$PATH}";
export MANPATH="${HOMEBREW_PREFIX}/share/man${MANPATH+:$MANPATH}";
export INFOPATH="${HOMEBREW_PREFIX}/share/info${INFOPATH+:$INFOPATH}";
export FPATH="${HOMEBREW_PREFIX}/share/zsh/site-functions${FPATH+:$FPATH}";

# MacTeX
export PATH="/Library/TeX/texbin${PATH+:$PATH}"
export MANPATH="/Library/TeX/Distributions/.DefaultTeX/Contents/Man${MANPATH+:$MANPATH}"
export INFOPATH="/Library/TeX/Distributions/.DefaultTeX/Contents/Info${INFOPATH+:$INFOPATH}"

# Java
export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"

# Opam
if [[ -r ~/.opam/opam-init/init.zsh ]]; then
  source ~/.opam/opam-init/init.zsh
fi

# OrbStack
if [[ -r ~/.orbstack/shell/init.zsh ]]; then
  source ~/.orbstack/shell/init.zsh
fi
